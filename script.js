function validarNombre() {

	setTimeout(function () {
		mostrarMensaje1('1.', true);
	}, 1000);
	setTimeout(function () {
		mostrarMensaje1('2..', true);
	}, 2000);
	setTimeout(function () {
		mostrarMensaje1('3...', true);
	}, 3000);

	setTimeout(function () {
		mostrarMensaje1('Podría escribirte los versos más hermosos este día... pero sólo te diré...', true);
	}, 4000);

	setTimeout(function () {
		mostrarMensaje2('¡Te amo mi vida, feliz cumpleaños!', true);
	}, 7000);
}

function mostrarMensaje1(mensaje, negrita = false) {
	var mensajeElement = document.getElementById('mensaje1');
	mensajeElement.innerHTML = negrita ? '<strong>' + mensaje + '</strong>' : mensaje;
}

function mostrarMensaje2(mensaje, negrita = false) {
	var mensajeElement = document.getElementById('mensaje2');
	var miFoto = document.getElementById("fotoAmor");
	miFoto.style.display = "block";
	miFoto.style.margin = "auto";
	mensajeElement.innerHTML = negrita ? '<strong>' + mensaje + '</strong>' : mensaje;
}
